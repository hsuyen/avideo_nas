<?php
/**
 * 符號連結更新作業
 *
 * 這項作業範圍包含:
 * 1 檢測不是連結的檔案, 列成清單檔(not_link.txt)
 * 2 檢測已失效之連結, 將之刪除, 並列成清單檔(target_lost.txt).
 * 3 檢測標的不是檔案的連結, 將之刪除, 並列成清單檔(not_file.txt)
 * 4 檢測標的不在定義範圍內的符號連結, 將之刪除, 並列成清單檔(undefined.txt).
 * 5 對於未連結的檔案, 建立清單檔(nolink.txt, nolink.json).
 * 5.1 承上, nolink.json 向 avideo web 查詢 avideo info 並更新 nolink.json內容.
 * 6 通過以上所有檢測者, 列成清單檔(ok.txt)
 *
 */

include __DIR__ . '/' . 'nas.inc.php';

// 自 avideo files 貯存端口, 取得 avideo files 清單.
$files = make_checking_files_list();

// 分析 符號連結 與 $files, 將回報結果輸出到檔案
$content = analyze_link_content($files);
foreach($content as $k => $text):
  file_put_contents("./tmp/" . $k.'.txt', $text);
endforeach;

// 建立 [未連結之檔案] 清單:
$nolink_list = array();
foreach($files as $file_path => $attr):
  if( $attr['link_flag'] == 0 ):
    $nolink .= $file_path . PHP_EOL;
    if ( !preg_match("/(avideo_batch_reg-.*)/",$file_path, $matches) )
      continue;
    
    $loc = str_replace('\\', '/', $matches[1]);
    $nolink_list[$loc] = array(
      'loc' => str_replace('\\', '/', $loc),
      'file_path' => str_replace('\\', '/', $file_path)
    );
  endif;  
endforeach;
file_put_contents("./tmp/" . 'nolink.txt', $nolink);

// 將未連結檔送上 avideo web 查其 info, 主要是抓 nid 跟 ext:
$nolink_json = "./tmp/" . 'nolink.json';
file_put_contents($nolink_json, json_encode2($nolink_list));
$cmd = "curl -F \"list_file=@$nolink_json\" ${conf['avideo_info_api']}";
echo $cmd.PHP_EOL;

$resp = shell_exec($cmd);
file_put_contents($nolink_json, $resp);

$items = json_decode($resp, true);
//逐一建立連結:
$ready_update = array();
foreach($items as $item):
  $target = $item['file_path'];
  $lfpath = sprintf("%s/%s.%s", $conf['symlink_base_dir'], $item['nid'], $item['ext']);
  printf("ln -s '%s' '%s'".PHP_EOL, $target, $lfpath);
  if ( symlink($target, $lfpath) ):
    $ready_update[] = array(
      'nid' => $item['nid'],
      'nas_server' => !empty($item['nas_server']) ? $item['nas_server'] : $conf['nas_server']
    );
  endif;
endforeach;

$ru_json = "./tmp/" . 'ready_update.json';
file_put_contents($ru_json, json_encode2($ready_update));

// 將 nas_server 欄位更新到 avideo web:
$php = __DIR__ . '/' . 'avideo_nas_update.php';
$cmd = "php -q $php $ru_json";
echo $cmd.PHP_EOL;
$resp = shell_exec($cmd);
  
/**
 * 分析符號連結, 並回報結果
 */
function analyze_link_content(&$files){
  
  global $conf, $argv;
  
  $symlink_base_dir = $conf['symlink_base_dir'];
  $handle = opendir($symlink_base_dir);
  $content = array(
    'not_link' => null,
    'target_lost' => null,
    'not_file' => null,
    'undefined' => null,
    'ok' => null
  );
  while (false !== ($entry = readdir($handle))) {
    if ( in_array($entry, array('.', '..')) )
      continue;
    
    $link = $symlink_base_dir . '/' . $entry;
    
    // 所有檔案應該都是符號連結. 非符號連結標記為 'not_link'.
    if( !is_link($link) ):
      $content['not_link'] .= $link . PHP_EOL;
      continue;
    endif;
    
    $file_path = @readlink($link);
    $file_path = str_replace('\\', '/', $file_path);
    
    // 連結標的已不存在:
    if( empty($file_path) ):
    
      // 做記錄
      $content['target_lost'] .= sprintf("%s %s", $entry, $file_path).PHP_EOL;
      
      //刪除符號連結:
      unlink($link);
      continue;
    endif;

    // 或是存在但卻不是檔案:
    if( !is_file($file_path) ):
      $content['not_file'] .= sprintf("%s %s", $entry, $file_path).PHP_EOL;
      
      //刪除符號連結:
      unlink($link);
      continue;
    endif;
    
    // 列出連結與檔案清單 - 連結到 [未被包含在endpoint所定義的範圍] 裏:
    $pass_flag = 0;
    foreach( $conf['endpoint_list'] as $ep ):
      if( substr($file_path,0, strlen($ep)) == $ep):
        $pass_flag = 1;
        break;
      endif;  
    endforeach;
    if( $pass_flag == 0):
      $content['undefined'] .= sprintf("%s %s", $entry, $file_path).PHP_EOL;
      
      //刪除符號連結:
      unlink($link);
      continue;
    endif;
    
    // 通過以上所有檢測:
    $files[$file_path]['link_flag'] = 1;
    $content['ok'] .= sprintf("%s %s", $entry, $file_path).PHP_EOL;
    if( substr($file_path, 0, 3) == 'F:/' )
      $nas_server = 'win7-5star';
    elseif( substr($file_path, 0, 3) == 'E:/' )
      $nas_server = 'win7';
    else
      $nas_server='nsa221';
    
    $ary['ok'][] = array(
      'nid'=> subtok($entry, '.', 0, 1),
      'nas_server' => $nas_server
    );
  }
  
  // 若有指定 "--update_nas_server", 則向 avideo web 更新 nas_server 欄位.
  if(!empty($argv[1]) && $argv[1] == '--update_nas_server'):
    $uf = './tmp/' . date('YmdHis') . rand(10,99) . '.json';
    file_put_contents($uf, json_encode2($ary['ok']));
    $cmd = "curl -F \"list_file=@$uf\" ${conf['avideo_reg_api']}";
    echo $cmd.PHP_EOL;
    echo shell_exec($cmd).PHP_EOL;
  endif;
    
  return $content;
}

/**
 * 自 avideo files 貯存端口, 取得 avideo files 清單.
 */
function make_checking_files_list(){
  global $conf;
  
  $files = array();
  $endpoint_list = $conf['endpoint_list'];
  foreach($endpoint_list as $label => $ep):
    $list = read_all_files($ep);
    foreach($list['files'] as $file_path):
      $file_path = str_replace('\\', '/', $file_path);
      $files[$file_path] = array(
        'file_path' => $file_path,
        'endpoint' => $label,
        'link_flag' => 0
      );
    endforeach;
  endforeach;
  return $files;
} 