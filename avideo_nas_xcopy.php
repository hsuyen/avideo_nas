﻿<?php
/**
 * 2020-02-06
 *
 * 處理 [影片檔清單格式](從 avideo server 產生) 的檔案: 產生批次複制指令稿.
 *
 * 此指令稿只適用於 win7 環境, 不適用於 ubuntu.
 *
 * win7環境下的命令模式搬移檔案, 還要考慮跨磁碟機、網芳、以及非英文命名
 * 之檔名是否相容於指令參數等議題, 非常複雜.
 *
 * 不敢保證通用性, 每次執行都需要人工介入確認執行結果的正確性.
 *
 * ubuntu 環境下應該會更單純.
 */ 
if(!is_file($argv[1])){
 die("%s is not a file.\n");
}

include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$target_base_dir = 'S:/5star/xcopy_porn_execellent';

if( !is_dir($target_base_dir) )
  die($target_base_dir . " is not a directory.");
  
$filesize = 0;
$cmds = '';
$entry_list = json_decode(file_get_contents($argv[1]), true);
usort($entry_list, "cmp");
foreach($entry_list as $entry):
  
  // 目的地檔案已存在, 則不處理.
  $fname = basename($entry['loc']);
  $target_path = $target_base_dir . '/' . $fname;
  if ( is_file($target_path) ){
    printf("REM %s has existed, pass.".PHP_EOL, $fname);
    continue;
  }
 
  foreach($conf['endpoint_list'] as $endpoint){
    
    $source_path = $endpoint . '/' . $entry['loc'];
    if( !is_file($source_path) ) {
      //printf("%s not existed.".PHP_EOL, $source_path);
      continue;
    }
    
    $source_path = str_replace('/', '\\', $source_path);
    $cmds .= sprintf("xcopy \"%s\" \"%s\" /s/i".PHP_EOL, $source_path, str_replace('/', '\\', $target_base_dir));
    $filesize += $entry['size'];
    break;
  }
endforeach;

// 產生所有指令稿:
echo $cmds;

printf("REM Total: %s bytes disk space required." . PHP_EOL, number_format($filesize));


function cmp($a, $b)
{
  return ($a['size']-$b['size']<0 ? -1 : 1);
}