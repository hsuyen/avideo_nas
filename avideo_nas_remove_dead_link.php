<?php
/**
 * 這是utf-8檔案
 * 刪除已失效的符號連結
 */

$conf['link_home'] = '/mnt/HD_a2/avideo';

$text = file_get_contents('ln.txt');
$lines = explode("\n", $text);
foreach($lines as $nid_ext):
  if(empty($nid_ext))
    continue;
  
  // 不是符號連結, 略過
  $link_path = $conf['link_home'] . '/' . $nid_ext;
  if ( !is_link($link_path) ) {
    echo "Pass: not link - $link_path".PHP_EOL;
    continue;
  }
    
  $file_path = readlink($link_path);
  
  if ( 'avideo_batch_reg-' == substr($file_path, 0, 17) ) {
    echo $file_path.PHP_EOL;
    continue;
  }
  
  // 若連結之目的檔已不存在, 則刪除連結.
  if( !is_file($file_path) ):
    echo $link_path.PHP_EOL;
    unlink($link_path);
  endif;
  
endforeach;
