<?php
/**
 *
 * 批次對 既已完成登錄的avideo檔案, 建立以 (nid.副檔案) 命名 之 符號連結,
 * 以便應用端透過有命名規則的 url 存取.
 *
 * @date:
 *   2014-02-08
 *
 */
include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$info = phase2_check($argv);
//生成 $batch_dir, $batch_path, $info_path, $entries 四個變數
foreach($info as $k=>$v){
  $$k = $v;
}

foreach($entries as &$entry):  
  if( false === entry_validation($entry, $batch_path) )
    continue;
  
  //建立符號連結:
  mk_avideo_symlink($entry);
endforeach;

//回寫log:
file_put_contents($info_path, json_encode2($entries));
