<?php
/**
 *
 * 批次登錄 avideo檔案
 * 
 *
 * @date:
 *   2015-05-16
 *     1 改寫取得檔案清單檔的邏輯; 使之可同時適用於 linux 及 win7 系統
 *
 *   2014-02-09
 *     1 調整程式架構. 尚未經過測試驗證; 留待下次執行時驗證.
 *
 *   2014-02-06
 *     1 開發測試完成. 執行結果符合預期.
 *
 */
include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$batch_info = phase1_check($argv);
$batch_path = $batch_info['batch_path'];

$lst_path = gen_avideo_list_file($batch_path);

if( !is_file($lst_path) )
  die("$lst_path created failed!\n");

/**
 * 產生清單檔, json格式
 * ex: avideo_batch_reg-2017-08-22.info
 * 用來描述要向 avideo web 註冊的檔案清單.
 *
 * @param $batch_path
 *  ex: "E:/video/share/avideo_batch_reg-2017-08-22"
 *
 * @return
 *  清單檔路徑, ex: "E:/video/share/avideo_batch_reg-2017-08-22.info"
 */
function gen_avideo_list_file($batch_path){

  global $conf;
  $allowed_avideo_exts = $conf['allowed_avideo_exts'];
  $batch_info_ext = $conf['batch_info_ext'];
  $nas_server = $conf['nas_server'];
  
  $batch_dir = basename($batch_path);
  $lst_path = $batch_path . $batch_info_ext;
  
  $ccc = get_entry_list($batch_path, $batch_dir, $allowed_avideo_exts);

  $entries = array();
  foreach($ccc as $c):
    $abs_fpath = dirname($batch_path) . '/' . $c;
    $duration = get_duration($abs_fpath);
    $entries[] = array(
      'loc' => $c,
      'title' => subtok(subtok($c, "/", -1), ".", 0, -1),
      'nid' =>0,
      'duration' => $duration,
      'size' => number_format(filesize($abs_fpath)),
      'ext' => subtok($c, ".", -1),
      'nas_server' => $nas_server
    );
  endforeach;
  
  file_put_contents($lst_path, json_encode2($entries));

  //chdir($cwd);
  
  return $lst_path;
}

/**
 * 取得 avideo檔案清單. 以本地之可檢索格式表示, 像這樣:
 * avideo_batch_reg-2017-08-22/avideo_12345/avideo_12345.avi
 * avideo_batch_reg-2017-08-22/pornhub_93040293/pornhub_93040293.mp4
 * ...
 */  
function get_entry_list($batch_path, $batch_dir, $allowed_avideo_exts) {
  $ccc = array();
  $sub_dirs = glob($batch_path.'/*');
  foreach($sub_dirs as $sub_dir):
    $items = glob($sub_dir.'/*');
    foreach($items as $item):
      if (in_array(subtok($item, '.', -1), $allowed_avideo_exts))
        $ccc[] = str_replace($batch_path, $batch_dir, $item);
    endforeach;
  endforeach;
  return $ccc;
}  
