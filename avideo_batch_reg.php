<?php
/**
 *
 * 批次登錄 avideo檔案
 * 
 *
 * @date:
 *   2015-05-16
 *     1 改寫取得檔案清單檔的邏輯; 使之可同時適用於 linux 及 win7 系統
 *
 *   2014-02-09
 *     1 調整程式架構. 尚未經過測試驗證; 留待下次執行時驗證.
 *
 *   2014-02-06
 *     1 開發測試完成. 執行結果符合預期.
 *
 */
include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$batch_info = phase1_check($argv);
$batch_path = $batch_info['batch_path'];
$batch_info_ext = $conf['batch_info_ext'];

$lst_path = $batch_path . $batch_info_ext;

if( !is_file($lst_path) )
  die("$lst_path lost!\n");

$cmd = "curl -F \"list_file=@$lst_path\" ${conf['avideo_reg_api']}";
echo $cmd;

$response = shell_exec($cmd);
file_put_contents($lst_path, $response);

// 2020-02-04:
// 更新 videosdb.videos 的 avideo_nid and avideo.field_avideo_photo
file_get_contents("https://vision-foto.net/videos/api/update_reg_avideo_nid_and_cover.php");

