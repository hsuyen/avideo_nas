<?php
/**
 *
 * 批次修改 avideo檔案的資訊, 並向 server 要求更新.
 * 
 *
 * @date:
 *   2017-02-21
 *     1 開發測試完成. 執行結果符合預期. 目前只適用於前綴為 "avideo_" 的檔案.
 *
 */
include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc';

$info = phase2_check($argv);
//生成 $batch_dir, $batch_path, $batch_log, $entries 四個變數
foreach($info as $k=>$v){$$k=$v;}

foreach($entries as &$entry):  
  if( false === entry_validation($entry, $batch_path) )continue;
  
  // 探測項目的儲存現況, 並修訂屬性值: duration, file_size,
  validate_avideo_attributes($entry);
endforeach;

//回寫log:
file_put_contents($batch_log, json_encode2($entries));

$cmd = "curl -F \"list_file=@$batch_log\" $avideo_reg_api";
echo $cmd;
//shell_exec($cmd);

/**
 * 產生清單檔
 */
function gen_avideo_list_file($batch_path){
  global $allowed_avideo_exts;
  
  $batch_dir = basename($batch_path);
  $lst_file = $batch_path.'.lst';
  
  $entry_list = get_entry_list($batch_path, $batch_dir, $allowed_avideo_exts);

  $entries = array();
  foreach($entry_list as $c):
    if ( false === entry_validation($c, $batch_path) ) continue;
    
    //執行:
  
  endforeach;
  
  file_put_contents($lst_file, json_encode2($entries));

  //chdir($cwd);
  
  return $lst_file;
}

/**
 * 修訂項目屬性值: duration, size
 */
function validate_avideo_attributes(&$entry){
  $entry['duration'] = get_duration($entry['path']);
  $entry['size'] = number_format(filesize($entry['path']));
}  
 
/**
 * 取得 avideo檔案 之 相對路徑之標準格式
 */  
function get_entry_list($batch_path, $batch_dir, $allowed_avideo_exts) {
  $entry_list = array();
  $sub_dirs = glob($batch_path.'/*');
  foreach($sub_dirs as $sub_dir):
    $items = glob($sub_dir.'/*');
    foreach($items as $item):
      if (in_array(subtok($item, '.', -1), $allowed_avideo_exts))
        $entry_list[] = str_replace($batch_path, $batch_dir, $item);
    endforeach;
  endforeach;
  return $entry_list;
}  
