<?php
/**
 * 2017-08-20
 *
 * 處理 [影片檔清單格式](從 avideo server 產生) 的檔案: 產生批次搬移指令稿.
 *
 * 此指令稿只適用於 win7 環境, 不適用於 ubuntu.
 *
 * win7環境下的命令模式搬移檔案, 還要考慮跨磁碟機、網芳、以及非英文命名
 * 之檔名是否相容於指令參數等議題, 非常複雜.
 *
 * 不敢保證通用性, 每次執行都需要人工介入確認執行結果的正確性.
 *
 * ubuntu 環境下應該會更單純.
 */ 
if(!is_file($argv[1])){
 die("%s is not a file.\n");
}

include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$target_base_dir = 'F:/videos/5star';

$mkdir_list = array();
$batch_tag_list = array();

$cmds = array(
  'mkdir' => '',
  'move' => '',
  'rmdir' => '',
  'symlink' => ''
);
$filesize = 0;
$entry_list = json_decode(file_get_contents($argv[1]), true);
foreach($entry_list as $entry):
  $entry['marked'] = false;
    
  /* 2017-08-25: 這個限制可能是個雞助, 暫時不採用.
  // 沒有"精存"分類, 則不處理.
  if ( '精存' != $entry['storage_level'] ){
    printf("Oute: %s".PHP_EOL, $entry['loc']);
    $entry['marked'] = true;
    continue;
  }
  */
  
  // 目的地檔案已存在, 則不處理. (不過仍製作符號連結命令.)
  $target_path = $target_base_dir . '/' . $entry['loc'];
  if ( is_file($target_path) ){
    printf("Pass: %s".PHP_EOL, $entry['loc']);
    $entry['marked'] = true;
    $cmds['symlink'] .= mk_symlink_cmd($target_path, "${entry['nid']}.${entry['ext']}");    
    continue;
  }
 
  foreach($conf['endpoint_list'] as $endpoint){
    if( !strcmp($endpoint, $target_base_dir) )
      continue;
    
    $source_path = $endpoint . '/' . $entry['loc'];
    if( !is_file($source_path) )
      continue;
    
    printf("Move: %s".PHP_EOL, $entry['loc']);
    $entry['marked'] = true;
    
    $batch_tag = dirname(dirname($entry['loc']));
    $batch_tag_list[$batch_tag] = $batch_tag;
    
    $target_dir = dirname(dirname($target_path));
    $source_dir = dirname($source_path);
    if( !is_dir($target_dir) && !in_array($target_dir, $mkdir_list) ) {
      $mkdir_list[] = $target_dir;
      $cmds['mkdir'] .= sprintf("mkdir \"%s\"".PHP_EOL, $target_dir);
    }
    
    if( !strcmp( substr($source_dir,0,3), substr($target_dir,0,3) ) ){
      // 以下指令在win7下只適用於來源與目的皆為同磁碟機的情境, 而且最快.
      // 跨磁碟機或是網芳的情境下會出現"存取被拒"的狀況:
      $cmds['move'] .= sprintf("move \"%s\" \"%s\"".PHP_EOL, $source_dir, $target_dir);
      $filesize += $entry['size'];
    }else{
      // 此為跨磁碟機的情境: 先用xcopy /s複製到目的, 再用rmdir /s/q 刪除來源.
      // 注意!! 來源目錄為網路芳鄰的場合下, "/"得先轉為"\", xcopy才識得.
      $source_dir = str_replace('/', '\\', $source_dir);
      $target_dir = dirname($target_path); // xcopy 與 move 對於目的目錄的作業方式不同.
      $cmds['move'] .= sprintf("xcopy \"%s\" \"%s\" /s/i".PHP_EOL, $source_dir, $target_dir);
      $cmds['rmdir'] .= sprintf("rmdir \"%s\" /s/q".PHP_EOL, $source_dir);
      $filesize += $entry['size'];
    }
    // 製作符號連結指令
    $cmds['symlink'] .= mk_symlink_cmd($target_path, "${entry['nid']}.${entry['ext']}");
  }
  if( !$entry['marked'] )
    printf("Lost: %s".PHP_EOL, $entry['loc']);
endforeach;

// 產生所有指令稿:
foreach($cmds as $k => $v):
  $f = "cmd_$k.bat";
  file_put_contents($f, $v);
  echo "$f created.".PHP_EOL;
endforeach;

printf("Total: %s bytes disk space required." . PHP_EOL, number_format($filesize));

/**
 * 製作符號連結指令
 */
function mk_symlink_cmd($target_path, $lfname) {
  return sprintf("php -q \"%s\\mk_symlink.php\" \"%s\" \"%s\"".PHP_EOL,
    __DIR__,
    $target_path,
    $lfname
  );
}