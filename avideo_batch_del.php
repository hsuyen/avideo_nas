<?php
/**
 * 處理 [影片檔清單格式] 的檔案: 批次刪除
 * 
 */ 
if(!is_file($argv[1])){
 die("%s is not a file.\n");
}

include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$entry_list = json_decode(file_get_contents($argv[1]), true);
foreach($entry_list as $entry):
  $entry['marked'] = false;

  foreach($endpoint_list as $endpoint){
   
    $target_path = $endpoint . '/' . $entry['loc'];
    if( !is_file($target_path) )
      continue;
    
    printf("Del: %s".PHP_EOL, $entry['loc']);
    $entry['marked'] = true;
    
    $target_path = dirname($target_path);
    if( !is_dir($target_path) ) {
      continue;
    }  
    $cmd .= sprintf("rmdir \"%s\" /s /q".PHP_EOL, $target_path);
  }
  if( !$entry['marked'] )
    printf("Lost: %s".PHP_EOL, $entry['loc']);
endforeach;

file_put_contents('cmd.bat', $cmd);
echo "cmd.bat created.".PHP_EOL;