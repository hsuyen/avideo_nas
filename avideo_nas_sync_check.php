<?php
/**
 * 2017-08-25
 * 檢查 是否有:
 *  1 server 端的 avideo node 已刪除,
 *    但 local 端的檔案與其link 仍殘存的狀況.
 *
 *  2 local 有哪些 nids 是在 server 端的篩選條件以外的.
 *
 */
include __DIR__ . '/nas.inc.php';

// 此應為: avideo web 的 avideo 頁面的 [製作被選取之avideo清單] 所產生的檔案.
if( empty($argv[1]) )
  die("nids_from_web file Required.");

$nids_from_web = $argv[1];
if (!is_file($nids_from_web))
  die("$nids_from_web is not file.".PHP_EOL);

// 此應為: avideo_nas_symlink_refresh.php 所產生之 ok.txt 檔案:
// 代表符號連結都是正常有效的.
if( empty($argv[2]) )
  die("nids_from_local file Required.");

$nids_from_local = $argv[2];
if (!is_file($nids_from_local))
  die("$nids_from_local is not file.".PHP_EOL);

// 取出自 avideo web 下載來的所有項目的 nid
$nids = array();
$items = json_decode(file_get_contents($nids_from_web), true);
foreach($items as $item):
  $nids[] = $item['nid'];
endforeach;

// 本地確認有效的所有符號連結的 nid
$lines = file($nids_from_local);

// 最後挑出: local 有, 但 avideo web 無的 nid;
// 這表示 local 與 avideo web 已發生不同步的狀況.
// 可能原因:
// 1 avideo 在 avideo web 的 node 已被刪除, 但 local 的 link 及 檔案 卻沒跟著刪.
// 2 自 avideo web 下載的清單是被條件篩選過的, 而 local nid 正好被篩選掉.
$stranges .= null;
$count = 0;
foreach($lines as $line):
  $nid = subtok(subtok($line, ' ', 0, 1), '.', 0, 1);
  //echo $nid.PHP_EOL;

  if ( !in_array($nid, $nids) ):
    $count++;
    $stranges .= $nid.PHP_EOL;
  endif;  
endforeach;
if($count >0)
  echo "strange $count nids found:" . PHP_EOL . $stranges;
else
  echo "Good job. No strange nids found.".PHP_EOL;

