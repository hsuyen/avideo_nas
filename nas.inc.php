<?php
include_once dirname(__FILE__)."/nas.conf";

/**
 * 為單檔建立符號連結
 * log:
 *   不要小看這個動作, 也是磨了好久, 付出很大的代價(毁損不少檔案!)才弄到目前的程度.
 *   符號連結要特別小心, 刪除不怕(不會傷害到原始檔案), 最怕的是不小心覆寫, 直接對原始檔案覆寫下去!
 */
function mk_avideo_symlink(&$entry){

  // 避免產生難以想像的誤刪災難, 以下判斷條件極度重要!!!
  // 若來源路徑未指定, 則不處理.
  if( empty($entry['path']) || !is_file($entry['path']) )
    return;

  //注意: 這兩個變數不一樣, 一個是要建立符號連結的家目錄, 一個則是url的根網址
  global $conf;
  $symlink_base_dir = $conf['symlink_base_dir'];
  $symlink_base_url = $conf['symlink_base_url'];
  
  $lfname = $entry['nid'].".".$entry['ext'];
  $lfpath = $symlink_base_dir.'/'.$lfname;

  // 先解除現在的檔案連結, 注意這個判斷條件極為重要! 避免誤刪檔案的意外.
  if ( is_link($lfpath) )
    unlink($lfpath);
    
  /**
   * 以下用法正確, 不必懷疑, 在 php 裏, 用 "/" 代表路徑之分隔字元,
   * 不管是在 linux 下或 windows 下皆一體適用.
   * 用 symlink 失敗, 最有可能就是 window cmd console 視窗不是以[系統管理者]的權限開的,
   * 改以[系統管理者]權限開啟即可正常運作.
   */  
  $ans = @symlink($entry['path'], $lfpath)?'Succeed':'Failed';
  printf("symlink: %s '%s' '%s'\n", $ans, $entry['path'], $lfpath);
}

/**
 * 取得影片播放長度(秒)
 */
function get_duration($file_path){
  if(!is_file($file_path)) return 0;

  $cmd = "php -r \"echo php_uname();\"";
  $output = shell_exec($cmd);  
  $os = substr($output, 0, 10);

  $ffmpeg = "D:/portables/ffmpeg/bin/ffprobe.exe";
  switch($os):
    case "Windows NT":
      $cmd = $ffmpeg . ' -i "' . $file_path . '" 2>&1';
      $output = shell_exec($cmd);
      $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
      if (preg_match($regex_duration, $output, $hms)) {
        $null = array_shift($hms); //print_r($hms);
      }
      break;
    default:
      $cmd = sprintf("ffmpeg -i '%s' 2>&1 | grep Duration | awk '{print $2}' | tr -d ,", $file_path);
      $string = shell_exec($cmd);
      $fs = explode(".", $string);
      $hms = explode(":", $fs[0]);
  endswitch;
  
  return $hms[0]*3600 + $hms[1]*60 + $hms[2];
}

/**
 * @param $dir
 *   絕對路徑
 * @param $allowed_exts
 *   符合之副檔名陣列
 *
 * @return
 *   檔案清單(不含路徑)
 */
function get_file_list($dir, $allowed_exts) {
  // Open a known directory, and proceed to read its contents
  if(!is_array($allowed_exts))
    $allowed_exts = (array)$allowed_exts;
    
  $files = array();
  if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
      while (($entry = readdir($dh)) !== false) {
        $ext = substr(strrchr($entry, '.'), 1);
        if( is_file($dir . '/' . $entry) && in_array($ext, $allowed_exts))
          $files[] = $entry;
      }
      closedir($dh);
    }
  }
  return $files;
}

/**
 * 找出 $batch_dir 的絕對路徑; 可能散布在不同的end point下.
 * 若在不同的 end point 都能找到 $batch_dir, 則以先尋獲者為主, 後面的略過.
 */
function get_batch_path($batch_dir){
  
  global $conf;
  $endpoint_list = $conf['endpoint_list'];
  $batch_info_ext = $conf['batch_info_ext'];
  
  $batch_path = null;
  foreach($endpoint_list as $endpoint):
    if( empty($endpoint) )continue;

    $bp = $endpoint . '/' . $batch_dir;
    if( is_dir($bp) ){
      $batch_path = $bp;
      if( is_file($endpoint . '/' . $batch_dir . $batch_info_ext) )
        return $batch_path;
    }
  endforeach;
  
  return $batch_path;
}
 
/**
 * 驗證 entry, 順便在確認其絕對路徑有效時, 加進path屬性
 */
function entry_validation(&$entry, $batch_path){
  $entry['path'] = null;
  
  //驗證 entry 是否有意義?
  if(empty($entry))return false;
  
  //驗證 nid 是否有意義?
  if( !is_numeric($entry['nid']) || $entry['nid']==0 )return false;

  //驗證檔案絕對路徑是否存在?
  $abs_path = dirname($batch_path).'/'.$entry['loc'];
  if(!is_file($abs_path)){
    //找別的掛載點看有沒有?
    global $endpoint_list;
    foreach($endpoint_list as $ep):
      $abs_path = $ep.'/'.$entry['loc'];
      if(is_file($abs_path)){
        $entry['path'] = $abs_path;     
        return true;        
      }
    endforeach;
    return false;
  }
  
  $entry['path'] = $abs_path;
  return true;
} 
/**
 *
 */
function phase2_check($argv) {
  
  global $conf;
  $batch_info_ext = $conf['batch_info_ext'];
  
  $info = phase1_check($argv);
  
  //驗證 [批次作業日誌檔] 是否存在?
  $batch_info = $info['batch_path'] . $batch_info_ext;
  if(!is_file($batch_info)) {
    die("$batch_info not lost!\n");
  }  

  //驗證 [批次作業記錄] 是否為空?
  $entries = json_decode(file_get_contents($batch_info), true);
  if(empty($entries))die("No entries found!\n");
  //print_r($entries);exit;
  
  return array_merge($info, array('info_path' => $batch_info, 'entries' => $entries));
}

/**
 * 第 1階段驗證: 驗證參數數量及格式
 * 只收 1個參數, 格式像這樣: avideo_batch_reg-2017-08-22
 */
function phase1_check($argv) {

  //驗證參數數目是否合法? 
  if( count($argv) != 2 )
    die("Exactly 1 parameter required!" . PHP_EOL .
        "Example: avideo_batch_reg-2014-02-07" . PHP_EOL
    );

  //驗證參數格式是否合法?
  $legal_pat = '/^avideo_batch_reg\-[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/';
  if( !preg_match($legal_pat, $argv[1], $matches))
    die("Parameter format Error!" . PHP_EOL .
        "Example: avideo_batch_reg-2014-02-07" . PHP_EOL
    );

  //驗證 [批次作業目錄] 是否存在?
  $batch_dir = $argv[1];
  $batch_path = get_batch_path($batch_dir);
  if(!$batch_path)
    die("$batch_dir not found in any endpoint!\n");
  
  $batch_info = array(
    'batch_dir' => $batch_dir,
    'batch_path' => $batch_path
  );
  
  return $batch_info;
}


/** 
* Finds path, relative to the given root folder, of all files and directories in the given directory and its sub-directories non recursively. 
* Will return an array of the form 
* array( 
*   'files' => [], 
*   'dirs'  => [], 
* ) 
* @author sreekumar 
* @param string $root 
* @result array 
*/ 
function read_all_files($root = '.'){ 
  $files  = array('files'=>array(), 'dirs'=>array()); 
  $directories  = array(); 
  $last_letter  = $root[strlen($root)-1]; 
  $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR; 
  
  $directories[]  = $root; 
  
  while (sizeof($directories)) { 
    $dir  = array_pop($directories); 
    if ($handle = opendir($dir)) { 
      while (false !== ($file = readdir($handle))) { 
        if ($file == '.' || $file == '..') { 
          continue; 
        } 
        $file  = $dir.$file; 
        if (is_dir($file)) { 
          $directory_path = $file.DIRECTORY_SEPARATOR; 
          array_push($directories, $directory_path); 
          $files['dirs'][]  = $directory_path; 
        } elseif (is_file($file)) { 
          $files['files'][]  = $file; 
        } 
      } 
      closedir($handle); 
    } 
  } 
  
  return $files; 
} 


/* 引自php官網:
 * subtok(string,chr,pos,len)
 *
 * chr = chr used to seperate tokens
 * pos = starting postion
 * len = length, if negative count back from right
 *
 *  subtok('a.b.c.d.e','.',0)     = 'a.b.c.d.e'
 *  subtok('a.b.c.d.e','.',0,2)   = 'a.b'
 *  subtok('a.b.c.d.e','.',2,1)   = 'c'
 *  subtok('a.b.c.d.e','.',2,-1)  = 'c.d'
 *  subtok('a.b.c.d.e','.',-4)    = 'b.c.d.e'
 *  subtok('a.b.c.d.e','.',-4,2)  = 'b.c'
 *  subtok('a.b.c.d.e','.',-4,-1) = 'b.c.d'
 */
function subtok($string,$chr,$pos,$len = NULL) {
  return implode($chr,array_slice(explode($chr,$string),$pos,$len));
}

/**
 *
 */
function echo2($text){
  echo $text . PHP_EOL;
}

/**
 *
 */
function json_encode2($entries) {
  
  if ( PHP_VERSION_ID >= 50400 )
    return json_encode($entries, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
  
  return json_encode($entries);
}  
