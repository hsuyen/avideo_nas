<?php
/**
 *
 * 更新 avideo 欄位值
 * 
 *
 * @date:
 *   2017-08-24
 *     1 吃 json檔, 並更新欄位值:
 *     2 json格式: list, nid為必備欄位. 其它可更新欄位有:
 *       duration, size, nas_server
 *
 */
include_once dirname(__FILE__).'/nas.conf';
include_once dirname(__FILE__).'/nas.inc.php';

$json_file = is_file($argv[1]) ? $argv[1] : getcwd() . '/' . $argv[1];

if ( !is_file($json_file) )
  die("Exactly 1 parameter required. Ex:" . PHP_EOL .
      "php -q avideo_nas_update.php [json_file]" .  PHP_EOL
  );

$cmd = "curl -F \"list_file=@$json_file\" ${conf['avideo_reg_api']}";
echo $cmd.PHP_EOL;
$resp = shell_exec($cmd);

echo $resp;