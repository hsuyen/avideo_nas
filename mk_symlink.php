<?php
include_once dirname(__FILE__).'/nas.conf';

if( count($argv) != 3 )
  die("Usage: php -q mk_symlink.php target_file symlink_name" . PHP_EOL .
      "Example: php -q mk_symlink.php F:\videos\5star\avideo_100\avideo_100.mp4 10322.mp4" . PHP_EOL
  );
  
$target = $argv[1];
$lfname = $argv[2];
$lfpath = $conf['symlink_base_dir'] . '/' . $lfname;

// 避免產生難以想像的誤刪災難, 以下判斷條件極度重要!!!
// 若來源路徑未指定, 則不處理.
if( empty($target) || !is_file($target) )
  die("\"$target\" is not a file.".PHP_EOL);
  
// 先解除現在的檔案連結, 注意這個判斷條件極為重要! 避免誤刪檔案的意外.
if ( is_link($lfpath) )
  unlink($lfpath);
  
/**
 * 以下用法正確, 不必懷疑, 在 php 裏, 用 "/" 代表路徑之分隔字元,
 * 不管是在 linux 下或 windows 下皆一體適用.
 * 用 symlink 失敗, 最有可能就是 window cmd console 視窗不是以[系統管理者]的權限開的,
 * 改以[系統管理者]權限開啟即可正常運作.
 */  
$ans = @symlink($target, $lfpath)?'Succeed':'Failed';
printf("symlink: %s '%s' '%s'\n", $ans, $target, $lfpath);
